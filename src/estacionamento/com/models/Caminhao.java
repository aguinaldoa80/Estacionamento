package estacionamento.com.models;

public class Caminhao extends Veiculo {
	private float capacidadeCarga;

	public float getCapacidadeCarga() {
		return capacidadeCarga;
	}

	public void setCapacidadeCarga(float capacidadeCarga) {
		this.capacidadeCarga = capacidadeCarga;
	}

	public void imprimeVeiculo() {
		super.imprimeVeiculo();
		System.out.println("CAPACIDADE DE CARGA: " + this.capacidadeCarga);
		System.out.println("____________________________________");
	}
}