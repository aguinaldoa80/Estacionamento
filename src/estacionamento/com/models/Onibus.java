package estacionamento.com.models;

public class Onibus extends Veiculo {
	private int capacidadePassageiros;

	public int getCapacidadePassageiros() {
		return capacidadePassageiros;
	}

	public void setCapacidadePassageiros(int capacidadePassageiros) {
		this.capacidadePassageiros = capacidadePassageiros;
	}

	public void imprimeVeiculo() {
		super.imprimeVeiculo();
		System.out.println("CAPACIDADE DE PASSAGEIROS: " + this.capacidadePassageiros);
		System.out.println("____________________________________");
	}
}
