package estacionamento.com.models;

public class Moto extends Veiculo {
	private int cilindradas;

	public int getCilindradas() {
		return cilindradas;
	}

	public void setCilindradas(int cilindradas) {
		this.cilindradas = cilindradas;
	}

	public void imprimeVeiculo() {
		super.imprimeVeiculo();
		System.out.println("CILINDRADAS: " + this.cilindradas);
		System.out.println("____________________________________");
	}
}
