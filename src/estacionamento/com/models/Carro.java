package estacionamento.com.models;

public class Carro extends Veiculo {
	private int qtdPortas;

	public int getQtdPortas() {
		return qtdPortas;
	}

	public void setQtdPortas(int qtdPortas) {
		this.qtdPortas = qtdPortas;
	}

	public void imprimeVeiculo() {
		super.imprimeVeiculo();
		System.out.println("QUANTIDADE DE PORTAS: " + this.qtdPortas);
		System.out.println("____________________________________");
	}
}