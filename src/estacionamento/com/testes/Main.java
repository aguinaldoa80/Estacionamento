package estacionamento.com.testes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import estacionamento.com.conexao.Conexao;
import estacionamento.com.models.Cliente;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Cliente cliente = new Cliente();
		cliente.setNome("Aguinaldo");
		cliente.setLogradouro("Rua exemplo um");
		cliente.setBairro("Bairo um");
		cliente.setNumero("239");
		cliente.setTelefone("038998982005");
		cliente.setMunicipio("Paracatu do Príncipe");
		cliente.setEstado("MG");
		cliente.setCep("38610000");
		String sql = "";
		Connection con = null;
		PreparedStatement stm = null;
		try {
			sql = "insert into clientes (nome, logradouro, numero, bairro, municipio, estado, cep, telefone) "
					+ "values (?,?,?,?,?,?,?,?)";
			con = Conexao.getConexao();
			stm = con.prepareStatement(sql);
			stm.setString(1, cliente.getNome());
			stm.setString(2, cliente.getLogradouro());
			stm.setString(3, cliente.getNumero());
			stm.setString(4, cliente.getBairro());
			stm.setString(5, cliente.getMunicipio());
			stm.setString(6, cliente.getEstado());
			stm.setString(7, cliente.getCep());
			stm.setString(8, cliente.getTelefone());
			stm.execute();
			stm.close();
			con.close();
			System.out.println("Cliente: "+cliente.getNome()+" cadastrado com sucesso!!!");
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			JOptionPane.showMessageDialog(null, "Erro ao gravar usuario no banco:\n " + e.getMessage());
		}
		ArrayList<Cliente> clientes = new ArrayList<>();
		ResultSet rs = null;
		try {
			sql = "select * from clientes";
			con = Conexao.getConexao();
			stm = con.prepareStatement(sql);
			rs = stm.executeQuery();
			while(rs.next()){
				Cliente cli = new Cliente();
				try {
					cli.setNome(rs.getString("nome"));
					cli.setBairro(rs.getString("bairro"));
					cli.setCep(rs.getString("cep"));
					cli.setCodigo(rs.getInt("codigo"));
					cli.setEstado(rs.getString("estado"));
					cli.setLogradouro(rs.getString("logradouro"));
					cli.setMunicipio(rs.getString("municipio"));
					cli.setNumero(rs.getString("numero"));
					cli.setTelefone(rs.getString("telefone"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				clientes.add(cli);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			rs.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			stm.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(Cliente c : clientes){
			System.out.println("código: "+c.getCodigo());
			System.out.println("nome: "+c.getNome());
			System.out.println("logradouro: "+c.getLogradouro());
			System.out.println("numero: "+c.getNumero());
			System.out.println("bairro: "+c.getBairro());
			System.out.println("municipio: "+c.getMunicipio());
			System.out.println("estado: "+c.getEstado());
			System.out.println("cep: "+c.getCep());
			System.out.println("telefone: "+c.getTelefone());
		}
	}	

}
