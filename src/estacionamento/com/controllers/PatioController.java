package estacionamento.com.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import estacionamento.com.models.Patio;

public class PatioController {

	private Patio patio;
	private ArrayList<Patio> listaPatio = new ArrayList<Patio>();
	Scanner sc = new Scanner(System.in);
	private int cont;

	public PatioController(ArrayList<Patio> listaPatio) {
		this.listaPatio = listaPatio;
		cont = 0;
	}

	public Patio getPatio() {
		return patio;
	}

	public void setPatio(Patio patio) {
		this.patio = patio;
	}

	public ArrayList<Patio> getListaPatio() {
		return listaPatio;
	}

	public void setListaPatio(ArrayList<Patio> listaPatio) {
		this.listaPatio = listaPatio;
	}

	public Patio retornaPatio(ArrayList<Patio> listaPatio) {
		int codigo = 0;
		int index = -1;
		boolean flag2 = false;
		String str;
		int resp = 0;
		do {
			boolean flag = false;

			System.out.println("DIGITE O C�DIGO DO P�TIO: ");
			str = sc.nextLine();

			try {
				codigo = Integer.parseInt(str);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;
			}
			for (int i = 0; i < listaPatio.size(); i++) {
				if (listaPatio.get(i).getCodigo() == codigo) {
					index = listaPatio.indexOf(listaPatio.get(i));
					flag = true;
					flag2 = true;
				}
			}
			if (!flag) {
				System.out.println("\nN�O H� P�TIO CADASTRADO COM O C�DIGO " + codigo);
				do {
					System.out.println("\nDESEJA CONTINUAR");
					System.out.println("1 - SIM");
					System.out.println("2 - N�O");
					System.out.print("OP��O: ");
					str = sc.nextLine();
					System.out.print("\n");
					try {
						resp = Integer.parseInt(str);
						if (resp <= 0 || resp > 2) {
							System.out.println("\nA OP��O DIGITADA N�O � V�LIDA.\n");
						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = true;
					}
				} while (resp <= 0 || resp > 2);
				if (resp == 1) {
					flag = false;

				} else {
					flag2 = true;

				}
			}

		} while (flag2 != true);
		if (index >= 0) {
			patio = listaPatio.get(index);
		}
		if (resp == 2) {
			return null;
		}
		return patio;
	}

	public void addPatio(ArrayList<Patio> listaPatio) {
		int codigo = 1;
		String nome, logradouro, numero, bairro, municipio, estado, cep, valor, str;
		int capacidadeDeVeiculos = 0;
		float valorDaDiaria = 0;
		boolean flag = false;
		System.out.println("\n--== [ CADASTRO DE P�TIO ] ==--\n");
		System.out.println("NOME: ");
		nome = sc.nextLine();
		System.out.println("LOGRADOURO: ");
		logradouro = sc.nextLine();
		System.out.println("N�MERO: ");
		numero = sc.nextLine();
		System.out.println("BAIRRO: ");
		bairro = sc.nextLine();
		System.out.println("MUNIC�PIO: ");
		municipio = sc.nextLine();
		System.out.println("ESTADO: ");
		estado = sc.nextLine();
		System.out.println("CEP: ");
		cep = sc.nextLine();
		do {
			flag = false;
			System.out.println("CAPACIDADE DE VE�CULOS: ");
			str = sc.nextLine();
			try {
				capacidadeDeVeiculos = Integer.parseInt(str);
				flag = true;
				if (capacidadeDeVeiculos <= 0) {
					System.out.println("\nA CAPACIDADE DE VE�CULOS DEVE SER MAIOR QUE ZERO.\n");
					flag = false;

				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = false;

			}
		} while (!flag);
		do {
			System.out.println("VALOR DA DI�RIA: ");
			valor = sc.nextLine();
			try {
				valorDaDiaria = Float.parseFloat(valor);
				flag = true;
				if (valorDaDiaria <= 0) {
					System.out.println("\nO VALOR DA DI�RIA PRECISA SER ACIMA DE ZERO.\n");
					flag = false;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = false;

			}
		} while (!flag);

		codigo += cont;
		Patio patio;
		patio = new Patio(codigo, nome, logradouro, numero, bairro, municipio, estado, cep, capacidadeDeVeiculos,
				valorDaDiaria);
		listaPatio.add(patio);
		cont++;

		System.out.println("\nP�TIO CADASTRADO COM SUCESSO.");
	}

	public void relatorioPatio(ArrayList<Patio> listaPatio) {
		System.out.println("\n--== [ RELAT�RIO ] ==--\n");
		if (listaPatio.isEmpty()) {
			System.out.println("\nN�O H� P�TIOS CADASTRADOS.\n");

		} else {
			Iterator<Patio> patioIterator = listaPatio.iterator();
			while (patioIterator.hasNext()) {
				Patio patio = patioIterator.next();
				patio.imprimePatio();
				System.out.println("__________________________________\n");
			}
			System.out.println("\nTOTAL DE P�TIOS CADASTRADOS " + listaPatio.size());

		}

	}

	public void alteraPatio(ArrayList<Patio> listaPatio) {
		int codigo = 0, resp = 0, op2 = 0, capV = 0;
		String str, str2, valor;
		float valorDaDiaria = 0;
		boolean flag = false;
		System.out.println("\n--== [ ALTERA��O DE P�TIO ] ==--\n");
		if (listaPatio.isEmpty()) {
			System.out.println("\nN�O H� P�TIOS CADASTRADOS.\n");
		} else {
			System.out.println("\nINFORME O C�DIGO DO P�TIO.\n");
			System.out.print("C�DIGO: ");
			str2 = sc.nextLine();
			try {
				codigo = Integer.parseInt(str2);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;

			}

			Iterator<Patio> patioIterator = listaPatio.iterator();
			while (patioIterator.hasNext()) {
				Patio patio = patioIterator.next();
				if (patio.getCodigo() == codigo) {
					patio.imprimePatio();
					flag = true;
					do {
						System.out.println("\n1 - ALTERAR TUDO");
						System.out.println("2 - ALTERAR INFORMA��O ESPEC�FICA");
						System.out.println("3 - VOLTAR");
						System.out.print("OP��O: ");
						str = sc.nextLine();
						try {
							resp = Integer.parseInt(str);
							if (resp <= 0 || resp > 3) {
								System.out.println("\nO N�MERO DIGITADO EST� FORA DO INTERVALO DO MENU.\n");

							}
						} catch (NumberFormatException e) {

							System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");

						}
						switch (resp) {

						case 1:
							System.out.println("\n--== [ ALTERAR INFORMA��ES P�TIO] ==--\n");
							System.out.println("NOME: ");
							patio.setNome(sc.nextLine());
							System.out.println("LOGRADOURO: ");
							patio.setLogradouro(sc.nextLine());
							System.out.println("N�MERO: ");
							patio.setNumero(sc.nextLine());
							System.out.println("BAIRRO: ");
							patio.setBairro(sc.nextLine());
							System.out.println("MUNIC�PIO: ");
							patio.setMunicipio(sc.nextLine());
							System.out.println("ESTADO: ");
							patio.setEstado(sc.nextLine());
							System.out.println("CEP: ");
							patio.setCep(sc.nextLine());
							do {
								flag = false;
								System.out.println("CAPACIDADE DE VE�CULOS: ");
								str = sc.nextLine();
								try {
									capV = Integer.parseInt(str);
									flag = true;
									if (capV <= 0) {
										System.out.println("\nA CAPACIDADE DE VE�CULOS DEVE SER MAIOR QUE ZERO.\n");
										flag = false;

									}
								} catch (NumberFormatException e) {

									System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
									flag = false;

								}
							} while (!flag);
							patio.setCapacidadeDeVeiculos(capV);
							do {
								System.out.println("VALOR DA DI�RIA: ");
								valor = sc.nextLine();
								try {
									valorDaDiaria = Float.parseFloat(valor);
									flag = true;
									if (valorDaDiaria <= 0) {
										System.out.println("\nO VALOR DA DI�RIA PRECISA SER ACIMA DE ZERO.\n");
										flag = false;
									}
								} catch (NumberFormatException e) {

									System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
									flag = false;

								}
							} while (!flag);
							patio.setValorDaDiaria(valorDaDiaria);
							System.out.println("LOTA��O: ");
							patio.setLotacao(sc.nextInt());
							sc.nextLine();
							System.out.println("\nP�TIO ALTERADO COM SUCESSO.\n");
							break;
						case 2:
							do {
								System.out.println("\n--== [ ALTERAR INFORMA��O ] ==--\n");
								System.out.println("1  - NOME");
								System.out.println("2  - LOGRADOURO");
								System.out.println("3  - N�MERO");
								System.out.println("4  - BAIRRO");
								System.out.println("5  - MUNIC�PIO");
								System.out.println("6  - ESTADO");
								System.out.println("7  - CEP");
								System.out.println("8  - CAPACIDADE DE VE�CULOS");
								System.out.println("9  - VALOR DA DI�RIA");
								System.out.println("10 - LOTA��O");
								System.out.println("11 - VOLTAR");
								System.out.print("OP��O: ");
								str = sc.nextLine();
								System.out.print("\n");
								try {
									op2 = Integer.parseInt(str);
									if (op2 <= 0 || op2 > 11) {
										System.out.println("\nO N�MERO DIGITADO EST� FORA DO INTERVALO DO MENU.\n");

									}
								} catch (NumberFormatException e) {

									System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");

								}
								switch (op2) {

								case 1:
									System.out.println("NOME: ");
									patio.setNome(sc.nextLine());
									System.out.println("\nNOME ALTERADO COM SUCESSO.");
									break;
								case 2:
									System.out.println("LOGRADOURO: ");
									patio.setLogradouro(sc.nextLine());
									System.out.println("\nLOGRADOURO ALTERADO COM SUCESSO.");
									break;
								case 3:
									System.out.println("N�MERO: ");
									patio.setNumero(sc.nextLine());
									System.out.println("\nN�MERO ALTERADO COM SUCESSO.");
									break;
								case 4:
									System.out.println("BAIRRO: ");
									patio.setBairro(sc.nextLine());
									System.out.println("\nBAIRRO ALTERADO COM SUCESSO.");
									break;
								case 5:
									System.out.println("MUNIC�PIO: ");
									patio.setMunicipio(sc.nextLine());
									System.out.println("\nMUNIC�PIO ALTERADO COM SUCESSO.");
									break;
								case 6:
									System.out.println("ESTADO: ");
									patio.setEstado(sc.nextLine());
									System.out.println("\nESTADO ALTERADO COM SUCESSO.");
									break;
								case 7:
									System.out.println("CEP: ");
									patio.setCep(sc.nextLine());
									System.out.println("\nCEP ALTERADO COM SUCESSO.");
									break;
								case 8:
									do {
										flag = false;
										System.out.println("CAPACIDADE DE VE�CULOS: ");
										str = sc.nextLine();
										try {
											capV = Integer.parseInt(str);
											flag = true;
											if (capV <= 0) {
												System.out.println(
														"\nA CAPACIDADE DE VE�CULOS DEVE SER MAIOR QUE ZERO.\n");
												flag = false;

											}
										} catch (NumberFormatException e) {

											System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
											flag = false;

										}
									} while (!flag);
									patio.setCapacidadeDeVeiculos(capV);
									System.out.println("\nCAPACIDADE DE VE�CULOS ALTERADO COM SUCESSO.");
									break;
								case 9:
									do {
										System.out.println("VALOR DA DI�RIA: ");
										valor = sc.nextLine();
										try {
											valorDaDiaria = Float.parseFloat(valor);
											flag = true;
											if (valorDaDiaria <= 0) {
												System.out.println("\nO VALOR DA DI�RIA PRECISA SER ACIMA DE ZERO.\n");
												flag = false;
											}
										} catch (NumberFormatException e) {

											System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
											flag = false;

										}
									} while (!flag);
									patio.setValorDaDiaria(valorDaDiaria);
									System.out.println("\nVALOR DA DI�RIA ALTERADO COM SUCESSO.");
									break;
								case 10:
									System.out.println("LOTA��O: ");
									patio.setLotacao(sc.nextInt());
									sc.nextLine();
									System.out.println("\nLOTA��O ALTERADA COM SUCESSO.");
									break;
								case 11:
									break;
								}

							} while (op2 != 11);
							flag = true;
							break;

						}

					} while (resp != 3);

				}

			}
			if (!flag) {
				System.out.println("\nN�O H� P�TIO CADASTRADO COM O C�DIGO " + codigo);
				System.out.print("\n");
			}
		}

	}

	public void removePatio(ArrayList<Patio> listaPatio) {
		int codigo = 0, resp = 0;
		boolean flag = false;
		String str;
		System.out.println("\n--== [ EXCLUS�O DE P�TIO ] ==--\n");
		if (listaPatio.isEmpty()) {
			System.out.println("\nN�O H� P�TIOS CADASTRADOS.\n");
		} else {
			System.out.println("\nINFORME O C�DIGO DO P�TIO.\n");
			System.out.print("C�DIGO: ");
			str = sc.nextLine();
			try {
				codigo = Integer.parseInt(str);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;

			}

			Iterator<Patio> patioIterator = listaPatio.iterator();
			while (patioIterator.hasNext()) {
				Patio patio = patioIterator.next();
				if (patio.getCodigo() == codigo) {
					patio.imprimePatio();
					System.out.println("\nEXCLUIR 1 - SIM / 2 - N�O");
					System.out.print("OP��O: ");
					str = sc.nextLine();
					try {
						resp = Integer.parseInt(str);
						if (resp <= 0) {
							System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
							flag = true;
						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = true;
					}
					if (resp == 1) {
						patioIterator.remove();
						flag = true;
						System.out.println("\nP�TIO EXCLUIDO COM SUCESSO.\n");
					} else {
						System.out.println("\nP�TIO N�O EXCLUIDO.\n");
						flag = true;
					}

				}

			}
			if (!flag) {
				System.out.println("\nN�O H� P�TIO CADASTRADO COM O C�DIGO " + codigo);
				System.out.print("\n");
			}
		}

	}

	public void consultaPatio(ArrayList<Patio> listaPatio) {
		int codigo = 0;
		boolean flag = false;
		String str;
		System.out.println("\n--== [ CONSULTAR P�TIO ] ==--\n");
		if (listaPatio.isEmpty()) {
			System.out.println("\nN�O H� P�TIOS CADASTRADOS.\n");
		} else {
			System.out.println("\nINFORME O C�DIGO DO P�TIO.\n");
			System.out.print("C�DIGO: ");
			str = sc.nextLine();
			try {
				codigo = Integer.parseInt(str);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;

			}

			for (Patio pat : listaPatio) {
				if (pat.getCodigo() == codigo) {
					pat.imprimePatio();
					flag = true;

				}
			}
			if (!flag) {
				System.out.println("\nN�O H� P�TIO CADASTRADO COM O C�DIGO " + codigo);
				System.out.print("\n");
			}
		}
	}
}