package estacionamento.com.controllers;

import java.util.ArrayList;
import java.util.Scanner;

import estacionamento.com.models.Moto;
import estacionamento.com.models.Veiculo;

public class MotoController {

	private Moto moto;
	int index = 0;
	private int cont = 200;
	Scanner sc = new Scanner(System.in);
	private int q = 0;

	public void adicionaMoto(ArrayList<Veiculo> listaVeiculo) {
		moto = new Moto();
		int codigo = 0, anoF = 0, anoM = 0, cilin = 0;
		boolean flag = false;
		String str;
		System.out.println("\n--== [ CADASTRO DE MOTO ] ==--\n");
		System.out.println("MARCA: ");
		moto.setMarca(sc.nextLine());
		System.out.println("MODELO: ");
		moto.setModelo(sc.nextLine());
		do {
			System.out.println("ANO DA FABRICA��O: ");
			str = sc.nextLine();
			try {
				anoF = Integer.parseInt(str);
				flag = true;
				if (anoF < 1869) {
					System.out.println("\nO ANO DE FABRICA��O DIGITADO N�O � V�LIDO.");
					System.out.println("O ANO DEVE SER IGUAL OU SUPERIOR A 1869.\n");
					flag = false;

				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = false;

			}
		} while (!flag);
		moto.setAnoFabricacao(anoF);
		do {
			flag = false;
			System.out.println("ANO DO MODELO: ");
			str = sc.nextLine();
			try {
				anoM = Integer.parseInt(str);
				flag = true;
				if (anoM < anoF) {
					System.out.println("\nO ANO DO MODELO N�O � V�LIDO.");
					System.out.println("O ANO DEVE SER IGUAL OU SUPERIOR AO ANO DE FABRICA��O.\n");
					flag = false;

				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = false;

			}
		} while (!flag);
		moto.setAnoModelo(anoM);
		System.out.println("CHASSI: ");
		moto.setChassi(sc.nextLine());
		System.out.println("PLACA: ");
		moto.setPlaca(sc.nextLine());
		do {
			System.out.println("CILINDRADAS: ");
			str = sc.nextLine();
			try {
				cilin = Integer.parseInt(str);
				flag = true;
				if (cilin <= 0) {
					System.out.println("\nA QUANTIDADE DE CILINDRADAS DEVE SER MAIOR 0.\n");
					flag = false;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = false;

			}
		} while (!flag);

		moto.setCilindradas(cilin);
		codigo += cont;
		moto.setCodigo(codigo);
		listaVeiculo.add(moto);
		System.out.println("\nMOTO CADASTRADA COM SUCESSO.");
		cont--;
		q++;
	}

	public int getQ() {
		return q;
	}

	public void setQ(int q) {
		this.q = q;
	}

	public void alteraMoto(ArrayList<Veiculo> listaVeiculo) {
		index = -1;
		int codigo = 0, anoF = 0, anoM = 0, cilin = 0;
		String str;
		boolean flag = false;
		System.out.println("--== [ ALTERA��O DE MOTO ] ==--");
		if (listaVeiculo.isEmpty()) {
			System.out.println("\nN�O H� VE�CULOS CADASTRADOS.\n");
		} else {
			System.out.println("\nINFORME O C�DIGO DA MOTO.\n");
			System.out.print("C�DIGO: ");
			str = sc.nextLine();
			try {
				codigo = Integer.parseInt(str);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;

			}

			for (int i = 0; i < listaVeiculo.size(); i++) {
				if (listaVeiculo.get(i).getCodigo() == codigo) {
					index = listaVeiculo.indexOf(listaVeiculo.get(i));
					listaVeiculo.get(i).imprimeVeiculo();
					flag = true;
				}
			}

			if (index >= 0) {
				System.out.println("\n--== [ ALTERAR INFORMA��ES MOTO] ==--\n");
				System.out.println("MARCA: ");
				moto.setMarca(sc.nextLine());
				System.out.println("MODELO: ");
				moto.setModelo(sc.nextLine());
				do {
					System.out.println("ANO DA FABRICA��O: ");
					str = sc.nextLine();
					try {
						anoF = Integer.parseInt(str);
						flag = true;
						if (anoF < 1930) {
							System.out.println("\nO ANO DE FABRICA��O DIGITADO N�O � V�LIDO.");
							System.out.println("O ANO DEVE SER SUPERIOR A 1930.\n");
							flag = false;

						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = false;

					}
				} while (!flag);
				moto.setAnoFabricacao(anoF);
				do {
					flag = false;
					System.out.println("ANO DO MODELO: ");
					str = sc.nextLine();
					try {
						anoM = Integer.parseInt(str);
						flag = true;
						if (anoM < anoF) {
							System.out.println("\nO ANO DO MODELO N�O � V�LIDO.");
							System.out.println("O ANO DEVE SER IGUAL OU SUPERIOR AO ANO DE FABRICA��O.\n");
							flag = false;

						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = false;

					}
				} while (!flag);
				moto.setAnoModelo(anoM);
				System.out.println("CHASSI: ");
				moto.setChassi(sc.nextLine());
				System.out.println("PLACA: ");
				moto.setPlaca(sc.nextLine());
				do {
					System.out.println("CILINDRADAS: ");
					str = sc.nextLine();
					try {
						cilin = Integer.parseInt(str);
						flag = true;
						if (cilin <= 0) {
							System.out.println("\nA QUANTIDADE DE CILINDRADAS DEVE SER MAIOR ZERO.\n");
							flag = false;
						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = false;

					}
				} while (!flag);

				moto.setCilindradas(cilin);

				listaVeiculo.set(index, moto);
				System.out.println("\nMOTO ALTERADA COM SUCESSO.\n");
			}
			if (!flag) {
				System.out.println("\nN�O H� MOTO CADASTRADA COM O C�DIGO " + codigo);
				System.out.print("\n");
			}
		}
	}

	public void removeMoto(ArrayList<Veiculo> listaVeiculo) {
		index = -1;
		int codigo = 0, resp = 0;
		String str;
		boolean flag = false;
		if (listaVeiculo.isEmpty()) {
			System.out.println("\nN�O H� VE�CULOS CADASTRADOS.\n");
		} else {
			System.out.println("\n--== [ EXCLUS�O DE MOTOS ] ==--\n");
			System.out.println("\nINFORME O C�DIGO DA MOTO.\n");
			System.out.print("C�DIGO: ");
			str = sc.nextLine();
			try {
				codigo = Integer.parseInt(str);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;
			}

			for (int i = 0; i < listaVeiculo.size(); i++) {
				if (listaVeiculo.get(i).getCodigo() == codigo) {
					index = listaVeiculo.indexOf(listaVeiculo.get(i));
					moto.imprimeVeiculo();
				}
			}

			if (index >= 0) {
				System.out.println("\nEXCLUIR 1 - SIM / 2 - N�O");
				System.out.print("OP��O: ");
				str = sc.nextLine();
				try {
					resp = Integer.parseInt(str);
					if (resp <= 0) {
						System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
						flag = true;
					}
				} catch (NumberFormatException e) {

					System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
					flag = true;
				}
				if (resp == 1) {
					listaVeiculo.remove(index);
					flag = true;
					System.out.println("\nMOTO EXCLUIDA COM SUCESSO.");
				} else {
					System.out.println("\nMOTO N�O EXCLU�DA.");
					flag = true;
				}

			}
			if (!flag) {
				System.out.println("\nN�O H� MOTO CADASTRADA COM O C�DIGO " + codigo);
				System.out.print("\n");
			}
		}
	}

}