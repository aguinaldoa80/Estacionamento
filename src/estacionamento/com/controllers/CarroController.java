package estacionamento.com.controllers;

import java.util.ArrayList;
import java.util.Scanner;

import estacionamento.com.models.Carro;
import estacionamento.com.models.Veiculo;

public class CarroController {
	private Carro carro;
	int index = 0;
	Scanner sc = new Scanner(System.in);
	private int cont = 400;
	private int q = 0;

	public CarroController() {

	}

	public int getQ() {
		return q;
	}

	public void setQ(int q) {
		this.q = q;
	}

	public void adicionaCarro(ArrayList<Veiculo> listaVeiculo) {
		carro = new Carro();
		int codigo = 0, anoF = 0, anoM = 0, qtdportas = 0;
		boolean flag = false;
		String str;
		System.out.println("\n--== [ CADASTRO DE CARRO ] ==--\n");
		System.out.println("MARCA: ");
		carro.setMarca(sc.nextLine());
		System.out.println("MODELO: ");
		carro.setModelo(sc.nextLine());
		do {
			System.out.println("ANO DA FABRICA��O: ");
			str = sc.nextLine();
			try {
				anoF = Integer.parseInt(str);
				flag = true;
				if (anoF < 1885) {
					System.out.println("\nO ANO DE FABRICA��O DIGITADO N�O � V�LIDO.");
					System.out.println("O ANO DEVE IGUAL OU SUPERIOR A 1885.\n");
					flag = false;

				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = false;

			}
		} while (!flag);
		carro.setAnoFabricacao(anoF);
		do {
			flag = false;
			System.out.println("ANO DO MODELO: ");
			str = sc.nextLine();
			try {
				anoM = Integer.parseInt(str);
				flag = true;
				if (anoM < anoF) {
					System.out.println("\nO ANO DO MODELO N�O � V�LIDO.");
					System.out.println("O ANO DEVE SER IGUAL OU SUPERIOR AO ANO DE FABRICA��O.\n");
					flag = false;

				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = false;

			}
		} while (!flag);
		carro.setAnoModelo(anoM);
		System.out.println("CHASSI: ");
		carro.setChassi(sc.nextLine());
		System.out.println("PLACA: ");
		carro.setPlaca(sc.nextLine());

		do {
			System.out.println("QUANTIDADE DE PORTAS: ");
			str = sc.nextLine();
			try {
				qtdportas = Integer.parseInt(str);
				flag = true;
				if (qtdportas < 1) {
					System.out.println("\nA QUANTIDADE DE PORTAS DEVE SER IGUAL OU MAIOR QUE UMA.\n");
					flag = false;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = false;

			}
		} while (!flag);

		carro.setQtdPortas(qtdportas);

		codigo += cont;
		carro.setCodigo(codigo);
		listaVeiculo.add(carro);
		System.out.println("\nCARRO CADASTRADO COM SUCESSO.");
		cont--;
		q++;
	}

	public void alteraCarro(ArrayList<Veiculo> listaVeiculo) {
		index = -1;
		int codigo = 0, anoF = 0, anoM = 0, qtdportas = 0;
		String str;
		boolean flag = false;
		System.out.println("--== [ ALTERA��O DE CARRO ] ==--");
		if (listaVeiculo.isEmpty()) {
			System.out.println("\nN�O H� VE�CULOS CADASTRADOS.\n");
		} else {

			System.out.println("\nINFORME O C�DIGO DO CARRO.\n");
			System.out.print("C�DIGO: ");
			str = sc.nextLine();
			try {
				codigo = Integer.parseInt(str);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;

			}

			for (int i = 0; i < listaVeiculo.size(); i++) {
				if (listaVeiculo.get(i).getCodigo() == codigo) {
					index = listaVeiculo.indexOf(listaVeiculo.get(i));
					listaVeiculo.get(i).imprimeVeiculo();
					flag = true;
				}
			}

			if (index >= 0) {
				System.out.println("\n--== [ ALTERAR INFORMA��ES CARRO ] ==--\n");
				System.out.println("MARCA: ");
				carro.setMarca(sc.nextLine());
				System.out.println("MODELO: ");
				carro.setModelo(sc.nextLine());
				do {
					System.out.println("ANO DA FABRICA��O: ");
					str = sc.nextLine();
					try {
						anoF = Integer.parseInt(str);
						flag = true;
						if (anoF < 1930) {
							System.out.println("\nO ANO DE FABRICA��O DIGITADO N�O � V�LIDO.");
							System.out.println("O ANO DEVE SER SUPERIOR A 1930.\n");
							flag = false;

						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = false;

					}
				} while (!flag);
				carro.setAnoFabricacao(anoF);
				do {
					flag = false;
					System.out.println("ANO DO MODELO: ");
					str = sc.nextLine();
					try {
						anoM = Integer.parseInt(str);
						flag = true;
						if (anoM < anoF) {
							System.out.println("\nO ANO DO MODELO N�O � V�LIDO.");
							System.out.println("O ANO DEVE SER IGUAL OU SUPERIOR AO ANO DE FABRICA��O.\n");
							flag = false;

						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = false;

					}
				} while (!flag);
				carro.setAnoModelo(anoM);
				System.out.println("CHASSI: ");
				carro.setChassi(sc.nextLine());
				System.out.println("PLACA: ");
				carro.setPlaca(sc.nextLine());
				do {
					System.out.println("QUANTIDADE DE PORTAS: ");
					str = sc.nextLine();
					try {
						qtdportas = Integer.parseInt(str);
						flag = true;
						if (qtdportas <= 1) {
							System.out.println("\nA QUANTIDADE DE PORTAS DEVE SER MAIOR QUE DUAS.\n");
							flag = false;
						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = false;

					}
				} while (!flag);

				carro.setQtdPortas(qtdportas);

				listaVeiculo.set(index, carro);
				System.out.println("\nCARRO ALTERADO COM SUCESSO.\n");
			}

			if (!flag) {
				System.out.println("\nN�O H� CARRO CADASTRADO COM O C�DIGO " + codigo);
				System.out.print("\n");
			}
		}
	}

	public void removeCarro(ArrayList<Veiculo> listaVeiculo) {
		index = -1;
		int codigo = 0, resp = 0;
		String str;
		boolean flag = false;
		if (listaVeiculo.isEmpty()) {
			System.out.println("\nN�O H� VE�CULOS CADASTRADOS.\n");
		} else {
			System.out.println("\n--== [ EXCLUS�O DE CARRO ] ==--\n");
			System.out.println("\nINFORME O C�DIGO DO CARRO.\n");
			System.out.print("C�DIGO: ");
			str = sc.nextLine();
			try {
				codigo = Integer.parseInt(str);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;
			}

			for (int i = 0; i < listaVeiculo.size(); i++) {
				if (listaVeiculo.get(i).getCodigo() == codigo) {
					index = listaVeiculo.indexOf(listaVeiculo.get(i));
					carro.imprimeVeiculo();
				}
			}

			if (index >= 0) {
				System.out.println("\nEXCLUIR 1 - SIM / 2 - N�O");
				System.out.print("OP��O: ");
				str = sc.nextLine();
				try {
					resp = Integer.parseInt(str);
					if (resp <= 0) {
						System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
						flag = true;
					}
				} catch (NumberFormatException e) {

					System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
					flag = true;
				}
				if (resp == 1) {
					listaVeiculo.remove(index);
					flag = true;
					System.out.println("\nCARRO EXCLUIDO COM SUCESSO.");
				} else {
					System.out.println("\nCARRO N�O EXCLUIDO.");
					flag = true;
				}

			}
			if (!flag) {
				System.out.println("\nN�O H� CARRO CADASTRADO COM O C�DIGO " + codigo);
				System.out.print("\n");
			}
		}
	}
}