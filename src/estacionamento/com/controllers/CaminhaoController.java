package estacionamento.com.controllers;

import java.util.ArrayList;
import java.util.Scanner;

import estacionamento.com.models.Caminhao;
import estacionamento.com.models.Veiculo;

public class CaminhaoController {

	private Caminhao caminhao;
	int index = 0;
	Scanner sc = new Scanner(System.in);
	private int cont = 100;
	private int q = 0;

	public void adicionaCaminhao(ArrayList<Veiculo> listaVeiculo) {
		caminhao = new Caminhao();
		int codigo = 0, anoF = 0, anoM = 0;
		float capCarg = 0;
		boolean flag = false;
		String str;
		System.out.println("\n--== [ CADASTRO DE CAMINH�O ] ==--\n");
		System.out.println("MARCA: ");
		caminhao.setMarca(sc.nextLine());
		System.out.println("MODELO: ");
		caminhao.setModelo(sc.nextLine());
		do {
			System.out.println("ANO DA FABRICA��O: ");
			str = sc.nextLine();
			try {
				anoF = Integer.parseInt(str);
				flag = true;
				if (anoF < 1885) {
					System.out.println("\nO ANO DE FABRICA��O DIGITADO N�O � V�LIDO.");
					System.out.println("O ANO DEVE SER IGUAL OU SUPERIOR A 1885.\n");
					flag = false;

				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = false;

			}
		} while (!flag);
		caminhao.setAnoFabricacao(anoF);
		do {
			flag = false;
			System.out.println("ANO DO MODELO: ");
			str = sc.nextLine();
			try {
				anoM = Integer.parseInt(str);
				flag = true;
				if (anoM < anoF) {
					System.out.println("\nO ANO DO MODELO N�O � V�LIDO.");
					System.out.println("O ANO DEVE SER IGUAL OU SUPERIOR AO ANO DE FABRICA��O.\n");
					flag = false;

				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = false;

			}
		} while (!flag);
		caminhao.setAnoModelo(anoM);
		System.out.println("CHASSI: ");
		caminhao.setChassi(sc.nextLine());
		System.out.println("PLACA: ");
		caminhao.setPlaca(sc.nextLine());
		do {
			System.out.println("CAPACIDADE DE CARGA: ");
			str = sc.nextLine();
			try {
				capCarg = Float.parseFloat(str);
				flag = true;
				if (capCarg <= 0) {
					System.out.println("\nA CAPACIDADE DE CARGA DEVE SER MAIOR QUE 0.\n");
					flag = false;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = false;

			}
		} while (!flag);

		caminhao.setCapacidadeCarga(capCarg);
		codigo += cont;
		caminhao.setCodigo(codigo);
		listaVeiculo.add(caminhao);
		System.out.println("\nCAMINH�O CADASTRADO COM SUCESSO.");
		cont--;
		q++;
	}

	public int getQ() {
		return q;
	}

	public void setQ(int q) {
		this.q = q;
	}

	public void alteraCaminhao(ArrayList<Veiculo> listaVeiculo) {
		index = -1;
		int codigo = 0, anoF = 0, anoM = 0;
		String str;
		boolean flag = false;
		float capCarg = 0;
		System.out.println("--== [ ALTERA��O DE CAMINH�O ] ==--");
		if (listaVeiculo.isEmpty()) {
			System.out.println("\nN�O H� CAMINH�ES CADASTRADOS.\n");
		} else {
			System.out.println("\nINFORME O C�DIGO DO CAMINH�O.\n");
			System.out.print("C�DIGO: ");
			str = sc.nextLine();
			try {
				codigo = Integer.parseInt(str);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;
			}
			for (int i = 0; i < listaVeiculo.size(); i++) {
				if (listaVeiculo.get(i).getCodigo() == codigo) {
					index = listaVeiculo.indexOf(listaVeiculo.get(i));
					listaVeiculo.get(i).imprimeVeiculo();
					flag = true;
				}
			}
			if (index >= 0) {
				System.out.println("\n--== [ ALTERAR INFORMA��ES CAMINH�O ] ==--\n");
				System.out.println("MARCA: ");
				caminhao.setMarca(sc.nextLine());
				System.out.println("MODELO: ");
				caminhao.setModelo(sc.nextLine());
				do {
					System.out.println("ANO DA FABRICA��O: ");
					str = sc.nextLine();
					try {
						anoF = Integer.parseInt(str);
						flag = true;
						if (anoF < 1930) {
							System.out.println("\nO ANO DE FABRICA��O DIGITADO N�O � V�LIDO.");
							System.out.println("O ANO DEVE SER SUPERIOR A 1930.\n");
							flag = false;
						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = false;
					}
				} while (!flag);
				caminhao.setAnoFabricacao(anoF);
				do {
					flag = false;
					System.out.println("ANO DO MODELO: ");
					str = sc.nextLine();
					try {
						anoM = Integer.parseInt(str);
						flag = true;
						if (anoM < anoF) {
							System.out.println("\nO ANO DO MODELO N�O � V�LIDO.");
							System.out.println("O ANO DEVE SER IGUAL OU SUPERIOR AO ANO DE FABRICA��O.\n");
							flag = false;
						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = false;

					}
				} while (!flag);
				caminhao.setAnoModelo(anoM);
				System.out.println("CHASSI: ");
				caminhao.setChassi(sc.nextLine());
				System.out.println("PLACA: ");
				caminhao.setPlaca(sc.nextLine());
				do {
					flag = false;
					System.out.println("CAPACIDADE DE CARGA: ");
					str = sc.nextLine();
					try {
						capCarg = Float.parseFloat(str);
						flag = true;
						if (capCarg <= 0) {
							System.out.println("\nA CAPACIDADE DE CARGA DEVE SER MAIOR QUE 0.\n");
							flag = false;
						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = false;

					}
				} while (!flag);

				caminhao.setCapacidadeCarga(capCarg);
				listaVeiculo.set(index, caminhao);
				System.out.println("\nCAMINH�O ALTERADO COM SUCESSO.\n");
			}
			if (!flag) {
				System.out.println("\nN�O H� CAMINH�O CADASTRADO COM O C�DIGO " + codigo);
				System.out.print("\n");
			}
		}
	}

	public void removeCaminhao(ArrayList<Veiculo> listaVeiculo) {
		index = -1;
		int codigo = 0, resp = 0;
		String str;
		boolean flag = false;
		if (listaVeiculo.isEmpty()) {
			System.out.println("\nN�O H� VE�CULOS CADASTRADOS.\n");
		} else {
			System.out.println("\n--== [ EXCLUS�O DE CAMINH�O ] ==--\n");
			System.out.println("\nINFORME O C�DIGO DO CAMINH�O.\n");
			System.out.print("C�DIGO: ");
			str = sc.nextLine();
			try {
				codigo = Integer.parseInt(str);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;
			}

			for (int i = 0; i < listaVeiculo.size(); i++) {
				if (listaVeiculo.get(i).getCodigo() == codigo) {
					index = listaVeiculo.indexOf(listaVeiculo.get(i));
					caminhao.imprimeVeiculo();
				}
			}

			if (index >= 0) {
				System.out.println("\nEXCLUIR 1 - SIM / 2 - N�O");
				System.out.print("OP��O: ");
				str = sc.nextLine();
				try {
					resp = Integer.parseInt(str);
					if (resp <= 0) {
						System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
						flag = true;
					}
				} catch (NumberFormatException e) {

					System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
					flag = true;
				}
				if (resp == 1) {
					listaVeiculo.remove(index);
					flag = true;
					System.out.println("\nCAMINH�O EXCLUIDO COM SUCESSO.");
				} else {
					System.out.println("\nCAMINH�O N�O EXCLUIDO.");
					flag = true;
				}

			}
			if (!flag) {
				System.out.println("\nN�O H� CAMINH�O CADASTRADO COM O C�DIGO " + codigo);
				System.out.print("\n");
			}
		}
	}

}