package estacionamento.com.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import estacionamento.com.models.Cliente;

public class ClienteController {
	private Cliente cliente;
	private ArrayList<Cliente> listaCliente = new ArrayList<Cliente>();
	Scanner sc = new Scanner(System.in);
	private int cont;

	public ClienteController(ArrayList<Cliente> listaCliente) {
		this.listaCliente = listaCliente;
		cont = 0;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public ArrayList<Cliente> getListaCliente() {
		return listaCliente;
	}

	public void setListaCliente(ArrayList<Cliente> listaCliente) {
		this.listaCliente = listaCliente;
	}

	public Cliente retornaCliente(ArrayList<Cliente> listaCliente) {
		int codigo = 0, index = -1;
		String str;
		boolean flag2 = false;
		int resp = 0;
		do {
			boolean flag = false;

			System.out.println("DIGITE O C�DIGO DO CLIENTE: ");
			str = sc.nextLine();

			try {
				codigo = Integer.parseInt(str);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;
			}

			for (int i = 0; i < listaCliente.size(); i++) {
				if (listaCliente.get(i).getCodigo() == codigo) {
					index = listaCliente.indexOf(listaCliente.get(i));
					flag = true;
					flag2 = true;

				}

			}

			if (!flag) {
				System.out.println("\nN�O H� CLIENTE CADASTRADO COM O C�DIGO " + codigo);
				do {
					System.out.println("\nDESEJA CONTINUAR");
					System.out.println("1 - SIM");
					System.out.println("2 - N�O");
					System.out.print("OP��O: ");
					str = sc.nextLine();
					System.out.print("\n");
					try {
						resp = Integer.parseInt(str);
						if (resp <= 0 || resp > 2) {
							System.out.println("\nA OP��O DIGITADA N�O � V�LIDA.\n");
						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = true;
					}
				} while (resp <= 0 || resp > 2);
				if (resp == 1) {
					flag = false;

				} else {
					flag2 = true;

				}
			}

		} while (flag2 != true);
		if (index >= 0) {
			cliente = listaCliente.get(index);
		}
		if (resp == 2) {
			return null;
		}
		return cliente;

	}

	public void addCliente(ArrayList<Cliente> listaCliente) {
		int codigo = 1;
		String nome, logradouro, numero, bairro, municipio, estado, cep, telefone;
		System.out.println("\n--== [ CADASTRO DE CLIENTE ] ==--\n");
		System.out.println("NOME: ");
		nome = sc.nextLine();
		System.out.println("LOGRADOURO: ");
		logradouro = sc.nextLine();
		System.out.println("N�MERO: ");
		numero = sc.nextLine();
		System.out.println("BAIRRO: ");
		bairro = sc.nextLine();
		System.out.println("MUNIC�PIO: ");
		municipio = sc.nextLine();
		System.out.println("ESTADO: ");
		estado = sc.nextLine();
		System.out.println("CEP: ");
		cep = sc.nextLine();
		System.out.println("TELEFONE: ");
		telefone = sc.nextLine();
		codigo += cont;
		Cliente cliente;
		cliente = new Cliente(codigo, nome, logradouro, numero, bairro, municipio, estado, cep, telefone);
		listaCliente.add(cliente);
		cont++;
		System.out.println("\nCLIENTE CADASTRADO COM SUCESSO.");

	}

	public void alteraCliente(ArrayList<Cliente> listaCliente) {
		int codigo = 0, resp = 0, op2 = 0;
		String str, str2;
		boolean flag = false;
		System.out.println("--== [ ALTERA��O DE CLIENTE ] ==--");
		if (listaCliente.isEmpty()) {
			System.out.println("\nN�O H� CLIENTES CADASTRADOS.\n");
		} else {
			System.out.println("\nINFORME O C�DIGO DO CLIENTE.\n");
			System.out.print("C�DIGO: ");
			str2 = sc.nextLine();
			try {
				codigo = Integer.parseInt(str2);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;

			}

			Iterator<Cliente> clienteIterator = listaCliente.iterator();
			while (clienteIterator.hasNext()) {
				Cliente cliente = clienteIterator.next();
				if (cliente.getCodigo() == codigo) {
					cliente.imprimeCliente();
					flag = true;
					do {
						System.out.println("\n1 - ALTERAR TUDO");
						System.out.println("2 - ALTERAR INFORMA��O ESPEC�FICA");
						System.out.println("3 - VOLTAR");
						System.out.print("OP��O: ");
						str = sc.nextLine();
						try {
							resp = Integer.parseInt(str);
							if (resp <= 0 || resp > 3) {
								System.out.println("\nO N�MERO DIGITADO EST� FORA DO INTERVALO DO MENU.\n");

							}
						} catch (NumberFormatException e) {

							System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
							resp = 0;

						}
						switch (resp) {

						case 1:
							System.out.println("\n--== [ ALTERAR INFORMA��ES ] ==--\n");
							System.out.println("NOME: ");
							cliente.setNome(sc.nextLine());
							System.out.println("LOGRADOURO: ");
							cliente.setLogradouro(sc.nextLine());
							System.out.println("N�MERO: ");
							cliente.setNumero(sc.nextLine());
							System.out.println("BAIRRO: ");
							cliente.setBairro(sc.nextLine());
							System.out.println("MUNIC�PIO: ");
							cliente.setMunicipio(sc.nextLine());
							System.out.println("ESTADO: ");
							cliente.setEstado(sc.nextLine());
							System.out.println("CEP: ");
							cliente.setCep(sc.nextLine());
							System.out.println("TELEFONE: ");
							cliente.setTelefone(sc.nextLine());
							System.out.println("\nCLIENTE ALTERADO COM SUCESSO.\n");
							break;
						case 2:
							do {
								System.out.println("\n--== [ ALTERAR INFORMA��O ] ==--\n");
								System.out.println("1 - NOME");
								System.out.println("2 - LOGRADOURO");
								System.out.println("3 - N�MERO");
								System.out.println("4 - BAIRRO");
								System.out.println("5 - MUNIC�PIO");
								System.out.println("6 - ESTADO");
								System.out.println("7 - CEP");
								System.out.println("8 - TELEFONE");
								System.out.println("9 - VOLTAR");
								System.out.print("OP��O: ");
								str = sc.nextLine();
								System.out.print("\n");
								try {
									op2 = Integer.parseInt(str);
									if (op2 <= 0 || op2 > 9) {
										System.out.println("\nO N�MERO DIGITADO EST� FORA DO INTERVALO DO MENU.\n");

									}
								} catch (NumberFormatException e) {

									System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
									op2 = 0;

								}
								switch (op2) {

								case 1:
									System.out.println("NOME: ");
									cliente.setNome(sc.nextLine());
									System.out.println("\nNOME ALTERADO COM SUCESSO.");
									break;
								case 2:
									System.out.println("LOGRADOURO: ");
									cliente.setLogradouro(sc.nextLine());
									System.out.println("\nLOGRADOURO ALTERADO COM SUCESSO.");
									break;
								case 3:
									System.out.println("N�MERO: ");
									cliente.setNumero(sc.nextLine());
									System.out.println("\nN�MERO ALTERADO COM SUCESSO.");
									break;
								case 4:
									System.out.println("BAIRRO: ");
									cliente.setBairro(sc.nextLine());
									System.out.println("\nBAIRRO ALTERADO COM SUCESSO.");
									break;
								case 5:
									System.out.println("MUNIC�PIO: ");
									cliente.setMunicipio(sc.nextLine());
									System.out.println("\nMUNIC�PIO ALTERADO COM SUCESSO.");
									break;
								case 6:
									System.out.println("ESTADO: ");
									cliente.setEstado(sc.nextLine());
									System.out.println("\nESTADO ALTERADO COM SUCESSO.");
									break;
								case 7:
									System.out.println("CEP: ");
									cliente.setCep(sc.nextLine());
									System.out.println("\nCEP ALTERADO COM SUCESSO.");
									break;
								case 8:
									System.out.println("TELEFONE: ");
									cliente.setTelefone(sc.nextLine());
									System.out.println("\nTELEFONE ALTERADO COM SUCESSO.");
									break;
								case 9:
									break;
								}

							} while (op2 != 9);
							flag = true;
							break;

						}

					} while (resp != 3);

				}

			}
			if (!flag) {
				System.out.println("\nN�O H� CLIENTE CADASTRADO COM O C�DIGO " + codigo);
				System.out.print("\n");
			}
		}
	}

	public void removeCliente(ArrayList<Cliente> listaCliente) {
		int codigo = 0, resp = 0;
		String str;
		boolean flag = false;
		System.out.println("\n--== [ EXCLUS�O DE CLIENTE ] ==--\n");
		if (listaCliente.isEmpty()) {
			System.out.println("\nN�O H� CLIENTES CADASTRADOS.\n");
		} else {
			System.out.println("\nINFORME O C�DIGO DO CLIENTE.\n");
			System.out.print("C�DIGO: ");
			str = sc.nextLine();
			try {
				codigo = Integer.parseInt(str);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;

			}

			Iterator<Cliente> clienteIterator = listaCliente.iterator();
			while (clienteIterator.hasNext()) {
				Cliente cliente = clienteIterator.next();
				if (cliente.getCodigo() == codigo) {
					cliente.imprimeCliente();
					System.out.println("\nEXCLUIR 1 - SIM / 2 - N�O");
					System.out.print("OP��O: ");
					str = sc.nextLine();
					try {
						resp = Integer.parseInt(str);
						if (resp <= 0) {
							System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
							flag = true;
						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = true;
					}
					if (resp == 1) {
						clienteIterator.remove();
						flag = true;
						System.out.println("\nCLIENTE EXCLUIDO COM SUCESSO.\n");
					} else {
						System.out.println("\nCLIENTE N�O EXCLUIDO.\n");
						flag = true;
					}

				}

			}
			if (!flag) {
				System.out.println("\nN�O H� CLIENTE CADASTRADO COM O C�DIGO " + codigo);
				System.out.print("\n");
			}
		}

	}

	public void consultaCliente(ArrayList<Cliente> listaCliente) {
		int codigo = 0;
		boolean flag = false;
		String str;
		System.out.println("\n--== [ CONSULTAR CLIENTE ] ==--\n");
		if (listaCliente.isEmpty()) {
			System.out.println("\nN�O H� CLIENTES CADASTRADOS.\n");
		} else {
			System.out.println("\nINFORME O C�DIGO DO CLIENTE.\n");
			System.out.print("C�DIGO: ");
			str = sc.nextLine();
			try {
				codigo = Integer.parseInt(str);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;

			}

			for (Cliente clie : listaCliente) {
				if (clie.getCodigo() == codigo) {
					clie.imprimeCliente();
					flag = true;

				}
			}
			if (!flag) {
				System.out.println("\nN�O H� CLIENTE CADASTRADO COM O C�DIGO " + codigo);
				System.out.print("\n");
			}

		}
	}

	public void relatorioCliente() {
		System.out.println("\n--== [ RELAT�RIO ] ==--\n");
		if (listaCliente.isEmpty()) {
			System.out.println("\nN�O H� CLIENTES CADASTRADOS.\n");

		} else {
			Iterator<Cliente> clienteIterator = listaCliente.iterator();
			while (clienteIterator.hasNext()) {
				Cliente cliente = clienteIterator.next();
				cliente.imprimeCliente();
				System.out.println("____________________________________\n");
			}
			System.out.println("\nTOTAL DE CLIENTES CADASTRADOS " + listaCliente.size());
		}
	}

}