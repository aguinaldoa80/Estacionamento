package estacionamento.com.controllers;

import java.util.ArrayList;
import java.util.Scanner;

import estacionamento.com.models.Onibus;
import estacionamento.com.models.Veiculo;

public class OnibusController {

	private Onibus onibus;
	int index = 0;
	private int cont = 300;
	private int q = 0;

	public int getQ() {
		return q;
	}

	public void setQ(int q) {
		this.q = q;
	}

	Scanner sc = new Scanner(System.in);

	public void adicionaOnibus(ArrayList<Veiculo> listaVeiculo) {
		onibus = new Onibus();
		int codigo = 0, anoF = 0, anoM = 0, caPass = 0;
		boolean flag = false;
		String str;
		System.out.println("\n--== [ CADASTRO DE �NIBUS ] ==--\n");
		System.out.println("MARCA: ");
		onibus.setMarca(sc.nextLine());
		System.out.println("MODELO: ");
		onibus.setModelo(sc.nextLine());
		do {
			System.out.println("ANO DA FABRICA��O: ");
			str = sc.nextLine();
			try {
				anoF = Integer.parseInt(str);
				flag = true;
				if (anoF < 1895) {
					System.out.println("\nO ANO DE FABRICA��O DIGITADO N�O � V�LIDO.");
					System.out.println("O ANO DEVE SER IGUAL OU SUPERIOR A 1895.\n");
					flag = false;

				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = false;

			}
		} while (!flag);
		onibus.setAnoFabricacao(anoF);
		do {
			flag = false;
			System.out.println("ANO DO MODELO: ");
			str = sc.nextLine();
			try {
				anoM = Integer.parseInt(str);
				flag = true;
				if (anoM < anoF) {
					System.out.println("\nO ANO DO MODELO N�O � V�LIDO.");
					System.out.println("O ANO DEVE SER IGUAL OU SUPERIOR AO ANO DE FABRICA��O.\n");
					flag = false;

				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = false;

			}
		} while (!flag);
		onibus.setAnoModelo(anoM);
		System.out.println("CHASSI: ");
		onibus.setChassi(sc.nextLine());
		System.out.println("PLACA: ");
		onibus.setPlaca(sc.nextLine());
		do {
			System.out.println("CAPACIDADE DE PASSAGEIROS: ");
			str = sc.nextLine();
			try {
				caPass = Integer.parseInt(str);
				flag = true;
				if (caPass <= 0) {
					System.out.println("\nA CAPACIDADE DE PASSAGEIROS DEVE SER MAIOR QUE 0.\n");
					flag = false;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = false;

			}
		} while (!flag);

		onibus.setCapacidadePassageiros(caPass);
		codigo += cont;
		onibus.setCodigo(codigo);
		listaVeiculo.add(onibus);
		System.out.println("\n�NIBUS CADASTRADO COM SUCESSO.");
		cont--;
		q++;
	}

	public void alteraOnibus(ArrayList<Veiculo> listaVeiculo) {
		index = -1;
		int codigo = 0, anoF = 0, anoM = 0, caPass = 0;
		String str;
		boolean flag = false;
		System.out.println("--== [ ALTERA��O DE �NIBUS ] ==--");
		if (listaVeiculo.isEmpty()) {
			System.out.println("\nN�O H� VE�CULOS CADASTRADOS.\n");
		} else {
			System.out.println("\nINFORME O C�DIGO DO �NIBUS.\n");
			System.out.print("C�DIGO: ");
			str = sc.nextLine();
			try {
				codigo = Integer.parseInt(str);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;

			}
			for (int i = 0; i < listaVeiculo.size(); i++) {
				if (listaVeiculo.get(i).getCodigo() == codigo) {
					index = listaVeiculo.indexOf(listaVeiculo.get(i));
					listaVeiculo.get(i).imprimeVeiculo();
					flag = true;
				}
			}

			if (index >= 0) {
				System.out.println("\n--== [ ALTERA��O DE �NIBUS ] ==--\n");
				System.out.println("MARCA: ");
				onibus.setMarca(sc.nextLine());
				System.out.println("MODELO: ");
				onibus.setModelo(sc.nextLine());
				do {
					System.out.println("ANO DA FABRICA��O: ");
					str = sc.nextLine();
					try {
						anoF = Integer.parseInt(str);
						flag = true;
						if (anoF < 1930) {
							System.out.println("\nO ANO DE FABRICA��O DIGITADO N�O � V�LIDO.");
							System.out.println("O ANO DEVE SER SUPERIOR A 1930.\n");
							flag = false;

						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = false;

					}
				} while (!flag);
				onibus.setAnoFabricacao(anoF);
				do {
					flag = false;
					System.out.println("ANO DO MODELO: ");
					str = sc.nextLine();
					try {
						anoM = Integer.parseInt(str);
						flag = true;
						if (anoM < anoF) {
							System.out.println("\nO ANO DO MODELO N�O � V�LIDO.");
							System.out.println("O ANO DEVE SER IGUAL OU SUPERIOR AO ANO DE FABRICA��O.\n");
							flag = false;

						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = false;

					}
				} while (!flag);
				onibus.setAnoModelo(anoM);
				System.out.println("CHASSI: ");
				onibus.setChassi(sc.nextLine());
				System.out.println("PLACA: ");
				onibus.setPlaca(sc.nextLine());
				do {
					System.out.println("CAPACIDADE DE PASSAGEIROS: ");
					str = sc.nextLine();
					try {
						caPass = Integer.parseInt(str);
						flag = true;
						if (caPass <= 15) {
							System.out.println("\nA CAPACIDADE DE PASSAGEIROS DEVE SER MAIOR QUE 15.\n");
							flag = false;
						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = false;

					}
				} while (!flag);

				onibus.setCapacidadePassageiros(caPass);

				listaVeiculo.set(index, onibus);
				System.out.println("\n�NIBUS ALTERADO COM SUCESSO.\n");
			}
			if (!flag) {
				System.out.println("\nN�O H� �NIBUS CADASTRADO COM O C�DIGO " + codigo);
				System.out.print("\n");
			}
		}
	}

	public void removeOnibus(ArrayList<Veiculo> listaVeiculo) {
		index = -1;
		int codigo = 0, resp = 0;
		String str;
		boolean flag = false;
		if (listaVeiculo.isEmpty()) {
			System.out.println("\nN�O H� VE�CULOS CADASTRADOS.\n");
		} else {
			System.out.println("\n--== [ EXCLUS�O DE �NIBUS ] ==--\n");
			System.out.println("\nINFORME O C�DIGO DO �NIBUS.\n");
			System.out.print("C�DIGO: ");
			str = sc.nextLine();
			try {
				codigo = Integer.parseInt(str);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;
			}

			for (int i = 0; i < listaVeiculo.size(); i++) {
				if (listaVeiculo.get(i).getCodigo() == codigo) {
					index = listaVeiculo.indexOf(listaVeiculo.get(i));
					onibus.imprimeVeiculo();
				}
			}

			if (index >= 0) {
				System.out.println("\nEXCLUIR 1 - SIM / 2 - N�O");
				System.out.print("OP��O: ");
				str = sc.nextLine();
				try {
					resp = Integer.parseInt(str);
					if (resp <= 0) {
						System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
						flag = true;
					}
				} catch (NumberFormatException e) {

					System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
					flag = true;
				}
				if (resp == 1) {
					listaVeiculo.remove(index);
					flag = true;
					System.out.println("\n�NIBUS EXCLUIDO COM SUCESSO.");
				} else {
					System.out.println("\n�NIBUS N�O EXCLUIDO.");
					flag = true;
				}

			}
			if (!flag) {
				System.out.println("\nN�O H� �NIBUS CADASTRADO COM O C�DIGO " + codigo);
				System.out.print("\n");
			}
		}
	}

}