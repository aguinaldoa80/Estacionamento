package estacionamento.com.controllers;

import java.util.ArrayList;
import java.util.Scanner;

import estacionamento.com.models.Caminhao;
import estacionamento.com.models.Carro;
import estacionamento.com.models.Moto;
import estacionamento.com.models.Onibus;
import estacionamento.com.models.Veiculo;

public class VeiculoController {
	private ArrayList<Veiculo> listaVeiculo = new ArrayList<Veiculo>();
	private CarroController f3 = new CarroController();
	private MotoController f4 = new MotoController();
	private CaminhaoController f5 = new CaminhaoController();
	private OnibusController f6 = new OnibusController();
	private Veiculo veiculo = new Veiculo();

	private int codigo;

	Scanner sc = new Scanner(System.in);

	public VeiculoController(ArrayList<Veiculo> listaVeiculo) {
		this.listaVeiculo = listaVeiculo;
	}

	public CarroController getF3() {
		return f3;
	}

	public void setF3(CarroController f3) {
		this.f3 = f3;
	}

	public MotoController getF4() {
		return f4;
	}

	public void setF4(MotoController f4) {
		this.f4 = f4;
	}

	public CaminhaoController getF5() {
		return f5;
	}

	public void setF5(CaminhaoController f5) {
		this.f5 = f5;
	}

	public OnibusController getF6() {
		return f6;
	}

	public void setF6(OnibusController f6) {
		this.f6 = f6;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public ArrayList<Veiculo> getListaVeiculo() {
		return listaVeiculo;
	}

	public void setListaVeiculo(ArrayList<Veiculo> listaVeiculo) {
		this.listaVeiculo = listaVeiculo;
	}

	public Veiculo retornaVeiculo(ArrayList<Veiculo> listaVeiculo) {
		int index = -1;
		boolean flag2 = false;
		String str;
		int resp = 0;
		do {
			boolean flag = false;

			System.out.println("DIGITE O C�DIGO DO VE�CULO: ");
			str = sc.nextLine();

			try {
				codigo = Integer.parseInt(str);
				if (codigo <= 0) {
					System.out.println("\nO C�DIGO DIGITADO N�O � V�LIDO.\n");
					flag = true;
				}
			} catch (NumberFormatException e) {

				System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
				flag = true;
			}

			for (int i = 0; i < listaVeiculo.size(); i++) {
				if (listaVeiculo.get(i).getCodigo() == codigo) {
					index = listaVeiculo.indexOf(listaVeiculo.get(i));
					flag = true;
					flag2 = true;
				}
			}
			if (!flag) {
				System.out.println("\nN�O H� VE�CULO CADASTRADO COM O C�DIGO " + codigo);
				do {
					System.out.println("\nDESEJA CONTINUAR");
					System.out.println("1 - SIM");
					System.out.println("2 - N�O");
					System.out.print("OP��O: ");
					str = sc.nextLine();
					System.out.print("\n");
					try {
						resp = Integer.parseInt(str);
						if (resp <= 0 || resp > 2) {
							System.out.println("\nA OP��O DIGITADA N�O � V�LIDA.\n");
						}
					} catch (NumberFormatException e) {

						System.out.println("\nLETRAS N�O S�O PERMITIDAS.\n");
						flag = true;
					}
				} while (resp <= 0 || resp > 2);
				if (resp == 1) {
					flag = false;

				} else {
					flag2 = true;

				}
			}

		} while (flag2 != true);
		if (index >= 0) {
			veiculo = listaVeiculo.get(index);
		}
		if (resp == 2) {
			return null;
		}
		return veiculo;
	}

	public void consultaVeiculo(ArrayList<Veiculo> listaVeiculo) {
		boolean flag = false;
		if (listaVeiculo.isEmpty()) {
			System.out.println("\nN�O H� VE�CULOS CADASTRADOS.\n");
			flag = true;
		} else {
			System.out.println("\nDIGITE O C�DIGO DO VE�CULO: ");
			codigo = sc.nextInt();
			sc.nextLine();
			for (Veiculo veiculo : listaVeiculo) {
				if (veiculo.getCodigo() == codigo) {
					veiculo.imprimeVeiculo();
					flag = true;
				}
			}
		}
		if (!flag) {
			System.out.println("\nN�O H� VEICULO CADASTRADO COM O C�DIGO " + codigo);
		}
	}

	public void relatorioCarro(ArrayList<Veiculo> listaVeiculo) {
		boolean flag = false;
		for (int i = 0; i < listaVeiculo.size(); i++) {
			if (listaVeiculo.get(i) instanceof Carro) {
				listaVeiculo.get(i).imprimeVeiculo();
				flag = true;

			}
		}
		System.out.println("\nTOTAL DE CARROS CADASTRADOS " + f3.getQ());
		System.out.println("\nTOTAL DE VE�CULOS CADASTRADOS " + listaVeiculo.size());
		if (!flag) {
			System.out.println("\nN�O H� CARROS CADASTRADOS.");
		}
	}

	public void relatorioMoto(ArrayList<Veiculo> listaVeiculo) {
		boolean flag = false;
		for (int i = 0; i < listaVeiculo.size(); i++) {
			if (listaVeiculo.get(i) instanceof Moto) {
				listaVeiculo.get(i).imprimeVeiculo();
				flag = true;
			}
		}
		System.out.println("\nTOTAL DE MOTOS CADASTRADAS " + f4.getQ());
		System.out.println("\nTOTAL DE VE�CULOS CADASTRADOS " + listaVeiculo.size());
		if (!flag) {
			System.out.println("\nN�O H� MOTOS CADASTRADAS.");
		}
	}

	public void relatorioCaminhao(ArrayList<Veiculo> listaVeiculo) {
		boolean flag = false;
		for (int i = 0; i < listaVeiculo.size(); i++) {
			if (listaVeiculo.get(i) instanceof Caminhao) {
				listaVeiculo.get(i).imprimeVeiculo();
				flag = true;

			}
		}
		System.out.println("\nTOTAL DE CAMINH�ES CADASTRADOS " + f5.getQ());
		System.out.println("\nTOTAL DE VE�CULOS CADASTRADOS " + listaVeiculo.size());
		if (!flag) {
			System.out.println("\nN�O H� CAMINH�ES CADASTRADOS.");
		}
	}

	public void relatorioOnibus(ArrayList<Veiculo> listaVeiculo) {
		boolean flag = false;
		for (int i = 0; i < listaVeiculo.size(); i++) {
			if (listaVeiculo.get(i) instanceof Onibus) {
				listaVeiculo.get(i).imprimeVeiculo();
				flag = true;
			}
		}
		System.out.println("\nTOTAL DE �NIBUS CADASTRADOS " + f6.getQ());
		System.out.println("\nTOTAL DE VE�CULOS CADASTRADOS " + listaVeiculo.size());
		if (!flag) {
			System.out.println("\nN�O H� �NIBUS CADASTRADOS.");
		}
	}
}